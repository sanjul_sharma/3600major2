#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdbool.h>


#define PATH		"path="
#define DELIM		":"

char* readLine(char* line, size_t len);
void split(char* input, char *argv[]);
bool handleQuotes(char* input,char *argv[]);
int checkpipe( char *input);
void executePipeCommand(char* input, char *argv[]);
void executeCommand(char* input, char *argv[]);
int runPipe(char *input);
void setPath(char* inputLine);
char* getPath();
void changedir(char*  dir);
int splitPipes(char* input, char *argv[]);
int checkRedirectChar( char *input);
bool getVariables(char* input);
void executeBashFile(char* filename);
void runBashLine(char* input,char* argv[]);

char* strcasestr(const char *haystack, const char *needle);

char *varNames[100];
char *varValues[100];
int varCount = 0;

int main(){ 
	char* input=NULL;
	char* input2;		
 	char *argv[50];
	char *argv2[30];
	char argv3[100];

	
	int i;	
	char* cwd;

  	while (1) { 
	   	cwd=getcwd(argv3,100);
		printf("%s",cwd);
    	printf(":~mosh:~$ "); //show a prompt		
		input=readLine(input, 0);		

		if(strcasestr(input, PATH) == input){
			setPath(input);			
		}else{	
			i=checkpipe(input);
			if(i==0){	
				executeCommand(input, argv);	
			}else{
				//printf("Pipe found\n");
				runPipe(input);			
			}
		}				
	}	
}

void runBashLine(char* input,char* argv[]){
	char argv3[100];
	char* cwd;
	cwd=getcwd(argv3,100);
	if(strcasestr(input, PATH) == input){
			setPath(input);			
	}
	else{
		int i;	
		i=checkpipe(input);
		if(i==0){	
			executeCommand(input, argv);	
		}else{
			//printf("Pipe found\n");
			runPipe(input);			
		}
	}	
}

/*split the input by space and fill to the given array*/
void split(char* input, char *argv[]){
	char* p;
	int argc;
	argc=0;
	p=strtok(input, " ");

	while(p!=NULL){
		argv[argc]=p;
		argc++;
		p=strtok(NULL, " ");
	}	
	argv[argc]='\0';
}

//this is to store variables if enterd by user like x=78
bool getVariables(char* input){
	char* p;
	int linwLen = strlen(input);
	bool retval=false;
	p=strtok(input, "=");
	//this means there were no = sign found in the input string
	if(linwLen == strlen(p)){
		return false;
	}
	else if(p!=NULL){
		varNames[varCount] = p;
		p=strtok(NULL, "=");
		if(p!=NULL){
			varValues[varCount] = p;
			varCount++;
			retval = true;
		}
	}	
	varNames[varCount]='\0';
	varValues[varCount]='\0';
	return retval;
}

//this is to replace any variables which is entered by user using $ notation
char* replaceVariables(char* startPoint,char* fullArg){
	char* firstPart; 
	// this is the first part of the argument which is prior to $ sign
	firstPart = strtok(fullArg, "$");

	//pointer diffrence is the length of 1st part
	int lenfirst = startPoint-firstPart;
	char delimit[]=" \'";
	char* p;
	char* remain;
	p=strtok(startPoint+1, delimit);
	if(p!=NULL){
		char* curVarValue;
		int i;
		for(i=0;i<varCount;i++){
			if(strcmp(varNames[i], p)==0){
				curVarValue = varValues[i];
				
				//this remain part contains the rest of the argument
				remain=strtok(NULL, "\0");
				int length;
				char *newString;

				//now after substituting the variable below code combines 
				//first part + variable + remain
				if(remain!=NULL){
					length= lenfirst+strlen(curVarValue)+strlen(remain);
					newString = malloc(length * sizeof(char));
					memcpy(newString,firstPart,strlen(firstPart));
					
					memcpy(newString+strlen(firstPart),curVarValue,strlen(curVarValue));
					memcpy(newString+strlen(firstPart)+strlen(curVarValue),remain,strlen(remain));
				}
				else{
					length= lenfirst+strlen(curVarValue);
					newString = malloc(length * sizeof(char));
					memcpy(newString,firstPart,strlen(firstPart));
					memcpy(newString+strlen(firstPart),curVarValue,strlen(curVarValue));
				}
				
				newString[length] = '\0';
				return newString;
			}
		}
	}	
	return NULL;
}

bool handleQuotes(char* input,char *argv[]){
	//if this is a varible assigning input
	//then getVariables() function will retunn true and 
	if(!getVariables(input)){
		//this part implements the state machine to figure out the arguments
		//then assign the args to Argv char pointer array
		char curArg[50] = "";
		int lenCurArg=0;
		bool s1=true;
		bool s2=false;
		bool s3=false;
		int argc=0;
		int charc=0;
		char curQuote='\0';
		bool isDoubleQWord = false;
		while(true){
			if(s1){
				//this part implements the logic related to state 1 of the state machine
				if(input[charc] != ' ' && input[charc] != '"' && input[charc] != '\''&&input[charc]!='`'){
					s1 =false;
					s2=true;
					curArg[lenCurArg] = input[charc];
					lenCurArg++;
					curArg[lenCurArg] = '\0';
				}
				else if(input[charc] == '"' || input[charc] == '\''||input[charc]=='`'){
					s1 =false;
					s3=true;
					curQuote= input[charc];
				}
			}
			else if(s2){
				//this part implements the logic related to state 2 of the state machine
				if(input[charc] == '\0'){
					char *a = malloc(lenCurArg * sizeof(char));
					memcpy(a,curArg,lenCurArg);
					if(isDoubleQWord){
						//this part is to check whether there are replacable variables since this argument has double quotes
						isDoubleQWord = false;
						  char* p; 
						//p points to the first occurance of the $ mark
						p = strstr(a, "$");
						if(p!=NULL){
							char* replacedWord=replaceVariables(p,a);
							if(replacedWord!=NULL){
								argv[argc] = replacedWord;
								free(a);
								curArg[0] = '\0';
								lenCurArg = 0;
								argc++;
								break;
							}
						}
						
						
					}
					argv[argc] = a;
					curArg[0] = '\0';
					lenCurArg = 0;
					argc++;
					break;
				}
				
				else if(input[charc] == ' '){
					s2 = false;
					s1 = true;
					
					char *a = malloc(lenCurArg * sizeof(char));
					memcpy(a,curArg,lenCurArg);
					if(isDoubleQWord){
						isDoubleQWord = false;
						  char* p; 
						// Find first occurrence of s2 in s1 
						p = strstr(a, "$");
						if(p!=NULL){
							char* replacedWord=replaceVariables(p,a);
							if(replacedWord!=NULL){
								argv[argc] = replacedWord;
								free(a);
								curArg[0] = '\0';
								lenCurArg = 0;
								argc++;
								break;
							}
						}
					}

					
					argv[argc] = a;
					curArg[0] = '\0';
					lenCurArg = 0;
					argc++;
				}
				else if(input[charc] == '"' || input[charc] == '\''||input[charc]=='`'){
					s2 =false;
					s3=true;
					curQuote= input[charc];
				}
				else{
					curArg[lenCurArg] = input[charc];
					lenCurArg++;
					curArg[lenCurArg] = '\0';
				}
				
			}
			else if(s3){
				//this part implements the logic related to state 3 of the state machine
				if(input[charc] == curQuote){
					s3=false;
					s2 = true;
					if (curQuote == '"'){
						isDoubleQWord = true;
					}

				}
				else if(input[charc] == '\n'){
					printf("ERROR mismatched quotes\n");
				}
				else{
					curArg[lenCurArg] = input[charc];
					lenCurArg++;
					curArg[lenCurArg] = '\0';
				}
			}
			charc++;
		}
		argv[argc] = '\0';
		return true;
	}
	return false;
}

/*split the input by space and fill to the given array*/
int splitPipes(char* input, char *argv[]){
	char* p;
	int argc;
	argc=0;
	p=strtok(input, "|");

	while(p!=NULL){
		argv[argc]=p;
		argc++;
		p=strtok(NULL, " ");
	}
	argv[argc]='\0';
	return argc;
}

//this function executes the bashfiles which runs using "./filename" command
void executeBashFile(char* filename){
	FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
	char *fileargv[50];

    fp = fopen(filename, "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
		*(line+strlen(line)-1)='\0';
		if(*(line) != '#'){
			runBashLine(line,fileargv);
		}
    }

    fclose(fp);
    if (line)
        free(line);
}

void executeCommand(char* input, char *argv[]){
	
	char *fargv[30];
	char* fname;
	char redirectChar;
	int index = checkRedirectChar(input);
	int isBg = 0;
	
	if(input[strlen ( input )-1]=='&'){
		isBg = 1;
		input[strlen ( input )-1]='\0';
	}
		
	if(index>0){
		redirectChar = input[index];
		fname=&input[index+1];
		input[index]='\0';
		split(fname, fargv);		
	}
	
	bool status=handleQuotes(input, argv);
	if(status){
		char* filename=strstr(argv[0], "./");
		//split(input, argv);
		//printf("first :%s\n",argv[0]);
		if(argv[0] == NULL) {
		}  
		else if(filename != NULL){
			executeBashFile(filename);
		} 	
		/* exit this shell when "exit" is inputed */
		else if(strcmp(argv[0], "exit") == 0) {      			
			exit(0);
		}
		else if(strcmp(argv[0], "cd") == 0) {   
			//printf("cd called\n");   			
			changedir(argv[1]);
		}
			
		pid_t pid = fork();
		if(pid == -1) {					 		
			perror("fork");
			exit(1);
		} else if(pid == 0) { 	
			if(redirectChar=='<'){
				int fd = open( fargv[0], O_RDONLY);
				close(0);
				dup(fd);
			} else if(redirectChar=='>'){
				int fd = open( fargv[0], O_WRONLY | O_CREAT | O_TRUNC, 0600);
				close(1);		
				dup(fd);
			}
			execvp(argv[0], argv);
			exit(0);
		} else {
			if(isBg){
			} else {
				wait(NULL);	
			}      					
		}	
	}
	
}

void executePipeCommand(char* input, char *argv[]){
	char *fargv[30];
	char* fname;
	char redirectChar;
	int index = checkRedirectChar(input);	
		
	if(index>0){
		redirectChar = input[index];
		fname=&input[index+1];
		input[index]='\0';
		split(fname, fargv);		
	}
	
	split(input, argv);
	
	if(argv[0] == NULL) {
		
	}   	
	/* exit this shell when "exit" is inputed */
	else if(strcmp(argv[0], "exit") == 0) {      			
		exit(0);
	}
	else if(strcmp(argv[0], "cd") == 0) {      			
		changedir(argv[1]);
	}
		
	if(redirectChar=='<'){
		int fd = open( fargv[0], O_RDONLY);
		close(0);
		dup(fd);
	} else if(redirectChar=='>'){
		int fd = open( fargv[0], O_WRONLY | O_CREAT | O_TRUNC, 0600);
		close(1);		
		dup(fd);
	}
	execvp(argv[0], argv);
	exit(0);
}

/* read a line */
char* readLine(char* line, size_t len){	
	getline(&line, &len, stdin);
	*(line+strlen(line)-1)='\0';
	return line;
}

/*return the array index of | character if any, otherwise 0*/
int checkpipe( char *input){
	int i = 0;
	int returnvalue = 0;
	for ( ; i < strlen ( input ) ; i++ ){
		if ( input[i] == '|' ){
			returnvalue = i;
			break;
		}	
	}
	return returnvalue;
}

int checkRedirectChar( char *input){
	int i = 0;
	int returnvalue = 0;
	for ( ; i < strlen ( input ) ; i++ ){
		if ( input[i] == '<' || input[i] == '>' ){
			returnvalue = i;
			break;
		}	
	}
	return returnvalue;
}

int runPipe(char *input){
	char *argv[100];
	char *argv2[50];
	int numPipes = splitPipes(input, argv);
	int fd[numPipes][2];
	int status;
	int isFirstPipe = 1;
	int pid[100] = {-1};
	int count = 0;
	int i;
		
	while (1){
		status = pipe(fd[count]);
		if (status < 0)		{
			printf("\npipe error");
			return -1;
		}		
		
		pid[count] = fork();
		
		if (pid[count] < 0)		{
			printf("\nError in fork");
			return -1;
		}else if (pid[count]) /*father code*/{
			if (argv[count+2]!='\0') {				
				count++;
				continue;
			}
			
			//close all opened pipes
			for (i = 0; i <= count; i++)			{
				close(fd[i][0]);
				close(fd[i][1]);
			}
			waitpid(pid[i], NULL, 0);
			/*if(!isConcurrency)			{
				for (i = 0; i <= count; i++)
					waitpid(pid[i], NULL, 0);
			}*/
			
			return 0;			
		}else	/*son code*/{
			if (count == 0)
				close(fd[0][0]);
			else
				dup2(fd[count-1][0], 0);
			
			if (argv[count+2]=='\0')
				close(fd[count][1]);
			else
				dup2(fd[count][1],1);
							
				
			//close all other opened pipes
			for (i = 0; i <= count; i++){
				close(fd[i][0]);
				close(fd[i][1]);
			}
			
			executePipeCommand(argv[count], argv2);
			
			//if execv failed
			printf("%s: failed to execute", argv[count]);
			return -1;
		}					
	}
}

/*add a given path to the environmental variable PATH*/
void setPath(char* inputLine){	
	char* pathIn = &inputLine[strlen(PATH)];
	char* oldPath = getenv("PATH");
	int lenPath = strlen(oldPath) + strlen(pathIn) + 1 + 1;
	char* pathOut = malloc(sizeof(char) * lenPath);
	strcat(pathOut, oldPath);
	strcat(pathOut, DELIM);
	strcat(pathOut, pathIn);
	pathOut[lenPath - 1] = '\0';	
	setenv("PATH", pathOut, 1);
	free(pathOut);
}

/*change directory if possible*/
void changedir(char* dir){
	if(dir=='\0'){
		chdir( "/home/" );
	}else{
		if(chdir(dir)==-1){
			printf("ERROR\n");
			printf("%s:Cannot find the directory\n",dir);
		}
	}
}

